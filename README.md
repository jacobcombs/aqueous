# Aqueous: Nothing short of pure brilliance!

One paragraph description goes here.

## Getting Started

**This section might be overkill**

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

NOT! But keeping this list around for now.

## Versioning

What would we even put here???

## Authors

* **Aaron Blumenfeld**
* **Jacob Combs**
* **Ashiq Rahman**
* **Justin Odle**
* **Jerry Seto**
* **Tim Yetter**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project. (Or don't, since this is not implemented yet.)

## License

This project is unlicensed, and not even in a legal sense. There has just been literally no thought put into this at all.

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
